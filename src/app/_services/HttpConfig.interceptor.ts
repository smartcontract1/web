import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {AuthService} from './auth.service';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {defaultError} from '../modules/share/utils/constants';
import {Router} from '@angular/router';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {
  constructor(
    private authService: AuthService,
    private toastrService: ToastrService,
    private translateService: TranslateService,
    private router: Router
  ) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.authService.token) {
      request = request.clone({headers: request.headers.set('sso', this.authService.token)});
    }

    return next.handle(request).pipe(
      catchError((err: any) => {
        switch (err.status) {
          case 401:
            console.error(err);
            this.authService.logout(true);
            break;
          case 500:
            this.toastrService.error(this.translateService.instant(defaultError));
            break;
          default:
            console.error(err);
        }
        const error = err.error.message || err.statusText;
        return throwError(err);
      })
    );
  }
}
