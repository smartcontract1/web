import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseUrl = environment.api_url;
  private apiVersion = environment.api_version;
  private apiUrl = this.baseUrl + this.apiVersion;

  constructor(
    private http: HttpClient,
  ) {}

  deleteUser(url, id, additionalUrl, data) {
    const options = {
      headers: {'Content-Type': 'application/json'},
      body: data,
    };
    return this.http.delete(this.apiUrl + url + '/' + id + additionalUrl, options);
  }

  addUser(url, id, additionalUrl, data) {
    const options = {
      headers: {'Content-Type': 'application/json'},
      body: data,
    };
    return this.http.put(this.apiUrl + url + '/' + id + additionalUrl, data);
  }

  searchUser(url, iin) {
    return this.http.get(`${this.apiUrl}${url}/${iin}/`);
  }
}
