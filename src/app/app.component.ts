import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {AuthService} from './_services/auth.service';
import {HttpRequest} from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styles: [`
    .wrapper {
      min-height: 100vh;
    }
    .page-body {
      min-height: 100%;
    }
  `]
})
export class AppComponent implements OnInit {
  constructor(
    private translate: TranslateService,
    public authService: AuthService,
  ) {
    this.initTranslate();
  }
  ngOnInit(): void { }

  initTranslate() {
    // Set the default language for translation strings, and the current language.
    this.translate.setDefaultLang('ru');
    this.initTranslateWords();
    const browserLang = this.translate.getBrowserLang();
    if (localStorage.getItem('language') === null) {
      if (browserLang && (browserLang === 'kk' || browserLang === 'en')) {
        this.translate.use(browserLang);
        localStorage.setItem('language', browserLang);
      } else {
        localStorage.setItem('language', 'ru');
        this.translate.use('ru'); // Set your language here
      }
    } else {
      this.translate.use(localStorage.getItem('language'));
    }
  }

  private initTranslateWords() {
    this.translate.instant('Contract Statuses.SIGNED');
    this.translate.instant('Contract Statuses.WAITING');
    this.translate.instant('Contract Statuses.IN_PROGRESS');
    this.translate.instant('Contract Statuses.REJECTED');
    this.translate.instant('Contract Statuses.ALL');
    this.translate.instant('Contract Statuses.DRAFT');
    this.translate.instant('Contract Statuses.FAILED');
    this.translate.instant('Contract Statuses.CANCELED');
    this.translate.instant('Contract Statuses.DELETED');
    this.translate.instant('Contract Statuses.SUCCESS');
    this.translate.instant('Contract Statuses.DONE');
    this.translate.instant('Contract Statuses.VERIFICATION');
    this.translate.instant('Contract Statuses.WAITING_FOR_CONFIRMATION');
    this.translate.instant('Contract Statuses.WAITING_FOR_APPROVAL');
    this.translate.instant('Contract Statuses.ARCHIVED');

    this.translate.instant('Participant');
    this.translate.instant('Initiator');

    this.translate.instant('User Statuses.USER_REJECTED');
    this.translate.instant('User Statuses.USER_SIGNED');
    this.translate.instant('User Statuses.USER_NOT_SIGNED');

    this.translate.instant('Statuses.CREATED');
    this.translate.instant('Statuses.REQUESTED');
    this.translate.instant('Statuses.REJECTED');
    this.translate.instant('Statuses.SUCCESS');
    this.translate.instant('Statuses.FINISHED');
    this.translate.instant('Statuses.USED');

    this.translate.instant('Request Statuses.REJECTED');

    this.translate.instant('Errors.Not selected');
    this.translate.instant('Errors.Service temporarily unavailable, try again later');
    this.translate.instant('Errors.CORRUPTED_XML');
    this.translate.instant('Errors.INVALID_SIGNER');
    this.translate.instant('Errors.To add a participant, you must obtain his consent to the collection and processing of personal data');
    this.translate.instant('Errors.You are trying to sign a contract with an authorization key (AUTH). To sign a contract, you must select a key with a prefix (RSA)');
    this.translate.instant('Errors.INVALID_USERNAME_OR_PASSWORD');
    this.translate.instant('Errors.INVALID_SIGNER');
    this.translate.instant('Errors.DOCUMENTS_DOESNT_MATCH');
    this.translate.instant('Errors.INVALID_SIGNATURE');
    this.translate.instant('Errors.TSP_ERROR');
    this.translate.instant('Errors.PERMISSION_DENIED');
    this.translate.instant('Errors.SERVICE_UNAVAILABLE');
    this.translate.instant('Errors.OBJECT_EXISTS');
    this.translate.instant('Errors.NOT_FOUND');
    this.translate.instant('Errors.BAD_REQUEST');
    this.translate.instant('Errors.UNAUTHORIZED');

    this.translate.instant('Confirm');
    this.translate.instant('Reject');
  }
}
