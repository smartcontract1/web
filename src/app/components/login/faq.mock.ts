export const FaqMock =  [
  {
    id: 1,
    show: false,
    question: {
      kk: '«Smart Contract» Сервисі. Бұл не?',
      ru: 'Сервис «Smart Contract». Что это?'
    },
    answer: {
      kk: 'Блокчейн технологиясындағы бизнес-қағидаларды басқарудың ақпараттық жүйесі ҚР ҰКО ЭЦҚ арқылы осындай шарттарға қол қоя отырып, шарттарды жасауға, оларды электрондық түрде сақтауға бағытталған. Шарттың түрі мен түріне қарай шарттың белгілі бір талаптары автоматты түрде орындалады (немесе қамтамасыз етіледі). «Smart contract» сервисі сайтта көрсетілген sc.egov.kz және eGov mobile мобильдік қосымшасы арқылы жүзеге асырылады.',
      ru: 'Информационная система управления бизнес-правилами в технологии Блокчейн направленная на заключение, хранение договоров в электронном виде с подписанием таких договоров посредством ЭЦП НУЦ РК.  В зависимости от типа и вида договора определённые условия контракта исполняются (или обеспечиваются) автоматически. Сервис «Smart contract» представлен на сайте sc.egov.kz и мобильном приложении eGov mobile.'
    }
  },{
    id: 1,
    show: false,
    question: {
      kk: 'Бұл қалай жұмыс істейді?',
      ru: 'Как это работает?'
    },
    answer: {
      kk: `<ol>
        <li>Авторландыру <br/>
          Сайтта sc.egov.kz «eGov көмегімен кіру» батырмасын басыңыз. Электрондық үкімет порталындағы өз ғылыми жазбаңыздың көмегімен ЭЦҚ арқылы авторизациядан өтіңіз. Немесе ЭЦҚ арқылы eGov mobile мобильді қосымшасында авторизациядан өтіп, «Менің шарттарым» сервисін таңдаңыз.
          <br/>Сервис 3 бөлімнен тұрады:
          <ul>
            <li>* Менің шарттарым-пайдаланушы жасаған шарттардың тізімі</li>
            <li>* Менің қатысуыммен жасалатын шарттар-пайдаланушы екінші тарап ретінде әрекет ететін шарттардың тізімі.</li>
            <li>* Жеке деректерді өңдеу, онда пайдаланушының жеке деректерін пайдалануға келісім беру үшін жіберілген және алынған сұраныстарды көруге болады. Мұнда сұрау мәртебесі көрсетіледі, келісім мәтінімен танысуға болады.</li>
          </ul>
          <br/>
        </li>
        <li>Келісім сұрау салу<br/>
          Таңдалған Шартты жасамас бұрын, жеке деректерді жинауға және өңдеуге екінші тараптың келісімін алу қажет.
          <br/>Бұл келесідей жасалады. «Келісімшарт жасау» батырмасын басыңыз, кейсті таңдаңыз, «Келісімді сұрату» батырмасын басыңыз, екінші Тараптың ЖСН көрсетіңіз, қажет болған жағдайда түсініктемені көрсетуге болады. «Жіберу» түймесін басыңыз.
          <br/>Екінші Тарапқа келісім беру қажеттілігі туралы хабарлама келеді.
          <br/><br/>
        </li>
        <li>Деректерді толтыру.<br/>
        Келісім берілгеннен кейін сіз келісімшартты құруға және барлық қажетті өрістерді толтыруға кірісе аласыз. Ол үшін таңдалған шарт атауының оң жағындағы көрсеткіні басу қажет.
        <br/>Деректерді толтырғаннан кейін келісімшартты жасау түймесін басыңыз.
        <br/><br/>
        </li>
        <li>Қол қою.<br/>
        Шартты толтырғаннан кейін «Шимай қағаз» мәртебесі беріледі, Сіз енгізілген деректерді қайта тексере аласыз, содан кейін шартқа қол қоя аласыз.
        <br/>Осыдан кейін екінші Тарап «Менің қатысуыммен жасалған шарттар» бөлімінде Шартты көре алады, талаптармен танысып, өз тарапынан шартқа қол қоя алады.
        </li>
        </ol>`,
      ru: `
      <ol>
        <li>Авторизация<br/>
          На сайте sc.egov.kz нажмите на кнопку "Войти с помощью eGov". Авторизуйтесь при помощи своей ученой записи на Портале электронного правительства посредством ЭЦП. Либо авторизуйтесь посредством ЭЦП в мобильном приложении eGov mobile и выберите сервис «Мои договора».
          <br/>Сервис состоит из 3 разделов:
          <ul>
            <li>* Мои договора – список договоров, созданных пользователем</li>
            <li>* Договора с моим участием – список договоров, в которых пользователь выступает в качестве второй стороны.</li>
            <li>* Обработка персональных данных, где можно увидеть отправленные и полученные запросы на предоставление согласия по использованию персональных данных пользователя. Здесь отображается статус запроса, можно ознакомиться с текстом согласия.</li>
          </ul>
          <br/>
        </li>
        <li>Запрос согласия<br/>
            Перед созданием выбранного Договора необходимо получить согласие второй стороны на сбор и обработку персональных данных.
          <br/>Делается это следующим образом. Нажмите на кнопку «Создать контракт», выберете кейс, нажмите на кнопку «Запросить согласие», укажите ИИН второй Стороны, при необходимости можно указать комментарий. Нажмите кнопку «Отправить».
          <br/>Второй стороне придет уведомление о необходимости предоставления согласия.
          <br/><br/>
        </li>
        <li>Заполнение данных.<br/>
        После того, как согласие будет предоставлено, можно перейти к созданию контракта и заполнению всех необходимых полей. Для этого необходимо нажать на стрелку справа от названия выбранного договора.
        <br/>После заполнения данных нажать на кнопку «Создать» контракт.
        <br/><br/>
        </li>
        <li>Подписание.<br/>
        После заполнения Договору будет присвоен статус "Черновик", вы сможете перепроверить введенные данные, после чего подписать Договор.
        <br/>После чего вторая сторона сможет увидеть Договор в разделе «Договора с моим участием», ознакомиться с условиями и подписать Договор со своей стороны.
        </li>
        </ol>`
    }
  },{
    id: 1,
    show: false,
    question: {
      kk: 'Мобильді нұсқа бар ма?',
      ru: 'Есть ли мобильная версия?'
    },
    answer: {
      kk: '«Smart Contract» сервисі сайтта көрсетілген sc.egov.kz және eGov mobile мобильдік қосымшасында да бар.',
      ru: 'Сервис «Smart Contract» представлен на  сайте sc.egov.kz и в мобильном приложении eGov mobile.\n'
    }
  },{
    id: 1,
    show: false,
    question: {
      kk: 'Қалай тіркелуге болады?',
      ru: 'Как пройти регистрацию?\n'
    },
    answer: {
      kk: '«Smart Contract» сервисін пайдалану үшін сізге «электрондық үкімет» порталында тіркелудің және ҚР ҰКО қолданыстағы ЭЦҚ болуы жеткілікті.\n',
      ru: 'Для того чтобы воспользоваться сервисом «Smart Contract» вам достаточно иметь регистрацию на портале «электронного правительства» и действующую ЭЦП НУЦ РК.\n'
    }
  },{
    id: 1,
    show: false,
    question: {
      kk: 'ЭЦҚ деген не?',
      ru: 'Что такое ЭЦП? '
    },
    answer: {
      kk: `Электрондық цифрлық қолтаңба-электрондық цифрлық қолтаңба құралдарымен жасалған және электрондық құжаттың анықтығын, оның тиесілілігін және мазмұнының өзгермейтінін растайтын электрондық цифрлық символдар жиынтығы;
<br/>Электрондық цифрлық қолтаңба қол қоюшы тұлғаның өз қолымен қойған қолымен тең және Қазақстан Республикасының «Электрондық құжат және электрондық цифрлық қолтаңба туралы» 2003 жылғы 7 қаңтардағы № 370-II Заңына сәйкес шарттар орындалған кезде бірдей заңдық салдарға әкеп соғады.
<br/>Электрондық цифрлық қолтаңба (ЭЦҚ) үшін тіркеу куәлігін алып, сіз оған сәйкес жабық кілттің көмегімен электрондық құжаттарға қол қоя аласыз. Электрондық құжаттағы сіздің қолтаңбаңызды (ЭЦҚ) тексеруді оған қоса берілген тиісті тіркеу куәлігі бойынша жүзеге асыруға болады.
`,
      ru: `Электронная цифровая подпись - набор электронных цифровых символов, созданный средствами электронной цифровой подписи и подтверждающий достоверность электронного документа, его принадлежность и неизменность содержания;
<br/>Электронная цифровая подпись равнозначна собственноручной подписи подписывающего лица и влечет одинаковые юридические последствия при выполнении условий согласно Закона Республики Казахстан от 7 января 2003 года № 370-II «Об электронном документе и электронной цифровой подписи».
<br/>Получив регистрационное свидетельство для электронной цифровой подписи (ЭЦП) вы сможете при помощи соответствующего ему закрытого ключа, подписывать электронные документы. Проверку вашей подписи (ЭЦП) на электронном документе можно будет осуществить по приложенному к нему соответствующему регистрационному свидетельству.
`
    }
  },{
    id: 1,
    show: false,
    question: {
      kk: 'ҚР ҰКО дегеніміз не?',
      ru: 'Что такое НУЦ РК?'
    },
    answer: {
      kk: '<a href="https://pki.gov.kz/">ҚР ҰКО</a> - мемлекеттік және мемлекеттік емес ақпараттық жүйелерде электрондық құжаттарды қалыптастыру үшін жеке немесе заңды тұлғаларға электрондық цифрлық қолтаңба құралдарын және тіркеу куәліктерін ұсынатын Ұлттық куәландырушы орталық.\n',
      ru: 'НУЦ РК  - <a href="https://pki.gov.kz/">Национальный удостоверяющий центр</a>, предоставляющий средства электронной цифровой подписи и регистрационные свидетельства физическим или юридическим лицам для формирования электронных документов в государственных и негосударственных информационных системах.\n'
    }
  },{
    id: 1,
    show: false,
    question: {
      kk: 'ЭЦҚ қалай алуға болады?',
      ru: 'Как получить ЭЦП?'
    },
    answer: {
      kk: `ЭЦҚ алу туралы толығырақ сілтеме бойынша біле аласыз <a href="https://egov.kz/cms/ru/services/pass_onlineecp">https://egov.kz/cms/ru/services/pass_onlineecp</a>`,
      ru: 'Подробнее о получении ЭЦП можете узнать по ссылке <a href="https://egov.kz/cms/ru/services/pass_onlineecp">https://egov.kz/cms/ru/services/pass_onlineecp</a>'
    }
  },{
    id: 1,
    show: false,
    question: {
      kk: 'Блокчейн дегеніміз не?',
      ru: 'Что такое Блокчейн?'
    },
    answer: {
      kk: 'Деректер бір-бірімен байланысты блоктарда сақталатын, осылайша деректердің өзгеріссіз сақталуын қамтамасыз ететін таратылған дерекқорлар мен жүйелерді іске асыру технологиясы. Өзгермейтіндіктің сенімділігі мен сақтаудың қол жетімділігіне бірнеше түйіндер мен осы түйіндердің блоктық жазбаларын тексеру процедуралары арасында тізілімнің (блокчейн тізбегінің) репликациясы арқылы қол жеткізіледі.',
      ru: 'Технология по реализации распределенных баз данных и систем, при которой данные хранятся в блоках, связанных друг с другом, тем самым обеспечивая неизменное хранение данных. Надежность неизменности и доступность хранения также достигается репликацией реестра (блокчейн-цепи) между несколькими узлами и процедурами верификации записи блоков данными узлами.\n'
    }
  },{
    id: 1,
    show: false,
    question: {
      kk: 'Шарттардың заңды күші бар ма?',
      ru: 'Имеют ли договора юридическую силу?'
    },
    answer: {
      kk: 'Иә, шарттар Электрондық құжат түрінде жасалғандықтан-ақпарат электрондық-цифрлық нысанда ұсынылған және электрондық цифрлық қолтаңба арқылы куәландырылған құжат.',
      ru: 'Да, поскольку договора заключаются в виде электронного документа – документа, в котором информация представлена в электронно-цифровой форме и удостоверена посредством электронной цифровой подписи.\n'
    }
  },{
    id: 1,
    show: false,
    question: {
      kk: 'Электрондық шарттарды сақтау мерзімі?',
      ru: 'Срок хранения электронных договоров?\n'
    },
    answer: {
      kk: '«Smart contract» арқылы жасалған құжаттар мерзімсіз сақталады.',
      ru: 'Документы, созданные посредством «Smart contract» хранятся бессрочно.\n'
    }
  },{
    id: 1,
    show: false,
    question: {
      kk: 'Неліктен дербес деректерді жинауға және өңдеуге келісімді сұрау керек?',
      ru: 'Зачем запрашивать согласие на сбор и обработку персональных данных?\n'
    },
    answer: {
      kk: 'Қазақстан Республикасының Дербес деректер туралы заңнамасына сәйкес адамның және азаматтың құқықтары мен бостандықтарын қорғауды қамтамасыз ету мақсатында дербес деректерді жинау және өңдеу субъектінің немесе оның заңды өкілінің келісімімен жүзеге асырылады.',
      ru: 'В соответствии с законодательством Республики Казахстан о персональных данных в целях обеспечения защиты прав и свобод человека и гражданина сбор и обработка персональных данных осуществляются с согласия субъекта или его законного представителя. \n'
    }
  },{
    id: 1,
    show: false,
    question: {
      kk: 'Мен дербес деректерді жинауға және өңдеуге келісім бермеймін бе?',
      ru: 'Могу ли я не давать согласие на сбор и обработку персональных данных?\n'
    },
    answer: {
      kk: 'Егер сізге дербес деректеріңізді алуға сұрау келсе, бірақ сіз өз келісіміңізді бергіңіз келмесе, онда «Дербес деректерді өңдеу» - «Алынған» бөлімінде сұрау салуды таңдаңыз және «Қабылдамау» басыңыз. Сұрау қабылданбайды және бастамашы сіздің деректеріңізді қолдана отырып шарт жасай алмайды.',
      ru: 'Если к вам пришел запрос на получение ваших персональных данных, но вы не хотите давать свое согласие, то в разделе «Обработка персональных данных» - «Полученные» выберите запрос и нажмите на кнопку «Отклонить». Запрос будет отклонен и инициатор не сможет создать договор, используя ваши данные.  \n'
    }
  },{
    id: 1,
    show: false,
    question: {
      kk: 'Шартқа өзгерістерді қалай енгізуге болады?',
      ru: 'Как внести изменения в Договор?\n'
    },
    answer: {
      kk: 'Егер шарт «Шимай қағаз» мәртебесінде болса, сіз «Деректерді өзгерту» батырмасын басып, қажетті өзгерістерді енгізе аласыз, содан кейін енгізілген деректердің дұрыстығына көз жеткізіп, шартқа қол қоя аласыз. Қол қойылған шарт өзгертуге жатпайды',
      ru: 'Если Договор находится в статусе «Черновик» вы можете нажать на кнопку «Изменить данные» и внести необходимые изменения, после чего еще раз убедившись в корректности внесённых данных подписать Договор. Подписанный Договор изменению не подлежит.\n'
    }
  },{
    id: 1,
    show: false,
    question: {
      kk: 'Жасалған шарттарға қалай және қашан қол жеткізе аламын?',
      ru: 'Как и когда я могу получить доступ в заключенным договорам?\n'
    },
    answer: {
      kk: 'Сіз Тараптардың бірі болып табылатын шарттар сіздің сайттағы аккаунтыңызда кез келген уақытта қолжетімді болады sc.egov.kz EGOV mobile мобильдік қосымшасы арқылы (менің шарттарымның сервисі).',
      ru: 'Договора, в которых вы являетесь одной из Сторон, доступны вам в любое время в вашем аккаунте на сайте sc.egov.kz и мобильном приложении eGov mobile (сервис Мои договора). '
    }
  },{
    id: 1,
    show: false,
    question: {
      kk: 'Үшінші тараптар менің шарттарыма қол жеткізе ала ма?',
      ru: 'Могут ли третьи лица получить доступ к моим договорам?\n'
    },
    answer: {
      kk: 'Жоқ, шарттар тек Шарт тараптары үшін қолжетімді',
      ru: 'Нет, договора доступны только для Сторон договора\n'
    }
  },{
    id: 1,
    show: false,
    question: {
      kk: 'Мен шартты өзімде сақтай аламын ба?',
      ru: 'Могу ли я сохранить договора у себя?\n'
    },
    answer: {
      kk: 'Ия, сіз шарттарды форматта сақтай аласыз .құрылғыдағы pdf',
      ru: 'Да, вы можете сохранить договора в формате .pdf на вашем устройстве\n'
    }
  },{
    id: 1,
    show: false,
    question: {
      kk: 'Мен немесе екінші тарап шартты жоя аламын ба?',
      ru: 'Могу ли я или вторая Сторона удалить договор?\n'
    },
    answer: {
      kk: 'Жоқ, өйткені шарттарды сақтау үшін Blockchain технологиясы қолданылады, жасалған шарттарды жою мүмкін емес.',
      ru: 'Нет, так как для хранения договоров используется технология блокчейн, удалить заключенные договора невозможно.'
    }
  },
];
