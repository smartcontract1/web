import {Component, EventEmitter, Input, Output, OnDestroy, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {TranslateService} from '@ngx-translate/core';
import {ContractService} from '../../../_services/contract.service';

@Component({
  selector: 'app-address-select',
  templateUrl: './address-select.component.html',
  styleUrls: ['./address-select.component.css']
})
export class AddressSelectComponent implements OnInit, OnDestroy {
  @Input() cities: any[] = [];
  @Output() onSelectFullAddress: EventEmitter<any> = new EventEmitter<any>();
  cityChildren: any[] = [];
  cityGrandChildren: any[] = [];
  selectedCity: any;
  selectedChild: any;
  selectedGrandChild: any;

  constructor(
    private activeModal: NgbActiveModal,
    public translateService: TranslateService,
    private contractService: ContractService,
  ) {
  }
  ngOnInit(): void {
    this.contractService.getCities().subscribe((res: any) => {
      console.log(res);
      this.cities = res.elements;
      this.cities.forEach((c) => c.loading = false);
    });
  }
  ngOnDestroy(): void {
  }
  closeModal() {
    this.activeModal.close();
  }

  selectFullAddress() {
    this.closeModal();
    const getName = (item) => {
      return item ? item.name[this.translateService.currentLang] : null;
    };
    const selectedObject = this.selectedGrandChild || this.selectedChild || this.selectedCity;
    const fullAddress = [getName(this.selectedCity), getName(this.selectedChild), getName(this.selectedGrandChild)]
      .filter(c => !!c)
      .join(', ');
    selectedObject.name[this.translateService.currentLang] = fullAddress;
    this.onSelectFullAddress.emit(selectedObject);
  }

  onSelectCity(city: any) {
    this.selectedCity = city;
    this.cityChildren = [];
    this.cityGrandChildren = [];
    this.selectedChild = undefined;
    this.selectedGrandChild = undefined;
    // TODO: remove this hardcode:
    console.log(city);
    if (!city.name.ru.includes('город')) {
      city.loading = true;
      this.contractService.getCityChildren(city.id)
        .subscribe((res: any) => {
          this.cityChildren = res && res.elements || [];
        },
          (err) => {},
          () => {
            city.loading = false;
          });
    }
  }

  onSelectCityChild(child: any) {
    this.selectedChild = child;
    child.loading = true;
    this.cityGrandChildren = [];
    this.selectedGrandChild = undefined;
    this.contractService.getCityChildren(child.id)
      .subscribe((res: any) => {
        this.cityGrandChildren = res && res.elements || [];
      },
        (err) => {
        },
        () => {
          child.loading = false;
        }
      );
  }

  onSelectGrandChild(grandChild: any) {
    this.selectedGrandChild = grandChild;
  }
}
