import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {LangChangeEvent, TranslateService} from '@ngx-translate/core';

import {ContractService} from '../../../_services/contract.service';
import {AuthService} from '../../../_services/auth.service';
import {NCAService} from '../../../_services/nca.service';
import {
  autoDownloadFile,
  capitalizeFullName,
  defaultDateTimeFormat,
  isDocumentWithPayment,
  PDF,
  transformDate
} from '../../share/utils/constants';
import {ContractStates, IContract, IDocument, ILocale, UserStatuses} from '../../../types/contract.types';
import {ToastrService} from 'ngx-toastr';
import {UsersComponent} from '../users/users.component';
import {UserService} from '../../../_services/user.service';
import {RequestService} from '../../request/request.service';
import {IRequest} from '../../../types/request.types';

@Component({
  selector: 'app-contract-item',
  templateUrl: './contract-item.component.html',
  styleUrls: ['./contract-item.component.css'],
  providers: [NgbModalConfig, NgbModal]
})
export class ContractItemComponent implements OnInit, OnDestroy {
  public mode: 'create' | 'edit' | 'read';
  public contractId: string;
  public contract: IContract;
  public documentId: string;
  public document: IDocument;

  public contractUsers = [];
  public documentFields;
  public agreements: IRequest[] = [];
  public initiator;
  public contractState: string;
  public contractForSign: any;
  public lang = 'ru';
  public isUsersShown = false;
  public loading = false;
  public currentLanguage = 'ru';
  public ifOpenSignModal = false;
  public userPermissions = {
    owner: false,
    signed: false,
    rejected: false,
  };
  public states = ContractStates;
  public dateTimeFormat = defaultDateTimeFormat;
  public loadingDownload = false;
  public shouldPay = false;
  public canPay = false;
  public paid = false;
  public rkCode;
  public payLoading = false;
  public waitingConductor = false;
  private ncaLoading: boolean;
  public selectedUser;

  public certificateChosenSubs;
  private paramsSubscription;
  private dataSubscription;
  private getXmlLoading: boolean;
  private agreementsLoading: boolean;

  constructor(
    public service: ContractService,
    private router: Router,
    private route: ActivatedRoute,
    public auth: AuthService,
    public nca: NCAService,
    private modalService: NgbModal,
    public translate: TranslateService,
    private toastrService: ToastrService,
    private location: Location,
    private userService: UserService,
    private requestService: RequestService
  ) {
    this.currentLanguage = this.translate.currentLang;
    translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.currentLanguage = event.lang;
    });
    this.certificateChosenSubs = this.nca.certificateChosen.subscribe(val => {
      if (val) {
        this.ifOpenSignModal = false;
      }
      this.getContractItem(true);
    });
  }

  ngOnInit() {
    this.paramsSubscription = this.route.params.subscribe((params) => {
      this.contractId = params.contractId;
      this.documentId = params.documentId;
      this.getData();
    });
    this.dataSubscription = this.route.data.subscribe(d => {
      this.mode = d.mode || 'create';
    });
  }
  ngOnDestroy(): void {
    // TODO: unsubscribe from all open subscriptions
    if (this.certificateChosenSubs) {
      this.certificateChosenSubs.unsubscribe();
    }
    this.paramsSubscription.unsubscribe();
    this.dataSubscription.unsubscribe();
  }
  getData() {
    if (this.auth.token && !this.auth.user) {
      this.loading = true;
      this.auth.getProfile().subscribe(res => {
        this.getData();
      });
      return;
    }
    if (this.contractId) {
      this.getContractItem();
    } else {
      this.mode = 'create'; // if there no contract id, it means mode is create
      this.getContractDocument(this.documentId);
    }
  }
  getContractDocument(docId) {
    if (!docId) {
      return;
    }
    this.loading = true;
    this.service.getContractDocument(docId, this.mode)
      .subscribe((res: IDocument) => {
        this.document = res;
        this.document.fields.forEach( field => {
          if (field.dataType === 'LOCAL_DATE' && this.documentFields) {
            this.documentFields[field.code] = transformDate(this.documentFields[field.code]);
          }
        });
        this.loading = false;
      });
  }
  getContractItem(updated = false) {
    this.loading = true;
    this.service.getContractItem(this.contractId, true).subscribe(res => {
      this.loading = false;
      this.parseContract(res);
      this.getWorkflow();
      this.getContractDocument(this.contract.documentId);
      this.getContractAgreements();
      if (updated) {
        this.service.addToUpdatedContracts(this.contract);
      }
    });
  }
  getContractAgreements() {
    const data = {
      contract_id: this.contractId
    };
    this.agreementsLoading = true;
    this.requestService.getContractAgreements(data).subscribe((res: any) => {
      this.agreementsLoading = false;
      this.agreements = res;
    });
  }
  getWorkflow() {
    this.rkCode = undefined;
    this.canPay = this.shouldPay = this.paid = false;
    if (isDocumentWithPayment(this.contract.documentId)) {
      this.service.getWorkflowState(this.contractId).subscribe((res: any) => {
          if (!res.data) {
            return;
          }
          const {tasks} = res.data;
          const rk = tasks.find((t: any) => t.taskType === 'get_rk_code');
          this.waitingConductor = rk && rk.status === 'IN_PROGRESS';
          const payment = tasks.find((t: any) => t.taskType === 'wait_for_mu_payment');
          this.canPay = payment && payment.status === 'IN_PROGRESS';
          this.paid = payment && payment.status === 'COMPLETED';
          this.shouldPay = this.canPay;
          if (this.documentFields.iin_pledgor) { // if pledge contract
            this.shouldPay = this.canPay && (this.documentFields.iin_pledgor === this.auth.user.iin);
          }
          this.rkCode = rk && !(payment && payment.status === 'COMPLETED') ? rk.outputData.rk_code : undefined;
        },
        error => {
          console.error(error);
        });
    }
  }
  convertFromExpToDecimal(key: any, value) {
    if (value.match('[-+]?[0-9]*\\.?[0-9]+([E][0-9]+)')) {
      const indexOfExp = value.indexOf('E');
      const result = value.slice(0, indexOfExp) * Math.pow(10, value.slice(indexOfExp + 1));
      // this.contract.fields[key] = result.toLocaleString().replace(/\s/g,''); // TODO check this
    }
  }
  parseContract(res: any) {
    this.contract = res;
    this.currentLanguage = this.contract.language || this.currentLanguage;
    for (const key in this.contract.fields) {
      this.convertFromExpToDecimal(key, this.contract.fields[key]);
    }
    this.documentFields = this.contract.fields;
    this.contractState = this.contract.state;
    this.initiator = {
      iin: this.contract.initiator.iin,
      fullName: this.contract.initiatorFullName
    };
    this.contractUsers = this.contract.parties.map(p => {
      const user = {
        iin: '',
        fullName: {},
        status: ''
      };
      user.iin = p.iin;
      user.fullName = this.contract.partiesFullNames[user.iin];
      user.status = UserStatuses.NOT_SIGNED;
      this.contract.confirmedParties.forEach( item => {
        if (item.iin === user.iin) {
          user.status = UserStatuses.SIGNED;
        }
      });
      this.contract.rejectedParties.forEach( item => {
        if (item.party.iin === user.iin) {
          user.status = UserStatuses.REJECTED;
        }
        if (item.party.iin === this.auth.user.iin) {
          this.userPermissions.rejected = true;
        }
      });
      return user;
    });
    this.setUserPermissions();
  }

  setUserPermissions() {
    this.userPermissions.owner = this.initiator.iin === this.auth.user.iin;
    this.userPermissions.signed = this.userPermissions.owner ?
      this.contract.initiatorSignatureProvided :
      !!this.contract.confirmedParties.find(p => p.iin.includes(this.auth.user.iin));
  }

  checkSignContract() {
    return (this.contractState === 'DRAFT' && this.contract.initiatorCanSign) ||
      (!this.userPermissions.owner && this.contract.partiesCanSign && !this.userPermissions.signed);
  }
  checkRejectContract() {
    return !this.userPermissions.owner && !this.userPermissions.signed && !this.userPermissions.rejected;
  }
  checkEditContract() {
    return this.userPermissions.owner && this.contractState === 'DRAFT';
  }
  checkManageUsers() {
    return this.userPermissions.owner && !this.userPermissions.signed;
  }
  onSignContract(content) {
    this.getXmlLoading = true;
    this.service.getContractForSign('/contracts', this.contractId)
      .subscribe(res => {
        this.getXmlLoading = false;
        this.contractForSign = res;
        this.nca.xmlToSign = res;
        this.openSignModal(content);
      });
  }
  openSignModal(content) {
    this.modalService.open(content, {centered: true});
  }
  closeSignModal() {
    this.ifOpenSignModal = false;
  }

  chooseCertificate() {
    this.ncaLoading = true;
    setTimeout(() => { // TODO: close only when ncaLayer opens
      this.ncaLoading = false;
    },2500);
    this.nca.selectSignType('SIGN', this.contractId, this.userPermissions.owner);
  }

  signContract() {
    const data = {
      initiatorSignature: {
        signature: this.nca.certificate,
        publicKey: ''
      }
    };
    this.service.signContract(this.contractId, this.userPermissions.owner, data)
      .subscribe(
        res => {
          this.ifOpenSignModal = false;
          this.getContractItem();
        },
        error => {
          console.error(error);
        }
      );
  }

  openRejectModal(content) {
    this.modalService.open(content, {backdrop: 'static'});
  }

  onRejectSignContract() {
    const data = { parties:
        [{
          party: {
            iin: this.auth.user.iin
          },
          reason: 'Reject with clicking Reject Button'
        }]};
    this.service.rejectSignContract(this.contractId, data)
      .subscribe(
        res => {
          this.getContractItem(true);
        },
        error => {
          this.toastrService.error(`Errors.${this.translate.instant(error)}`);
        }
      );
    this.modalService.dismissAll();
  }

  onEditContract() {
    this.router.navigate([`/contracts/edit/${this.documentId}/${this.contractId}`]);
  }

  onDownloadContract() {
    (this.contractState !== 'DRAFT') ? this.downloadContract() : this.previewContract();
  }

  previewContract() {
    this.loadingDownload = true;
    const data = {
      contractId: this.contractId,
      locale: this.contract.language
    };
    this.service.previewContract('/documents/generator/previewPDF', data)
      .subscribe(
        res => {
          this.loadingDownload = false;
          this.previewDocument(res);
        }, error => {
          this.loadingDownload = false;
        }
      );
  }

  downloadContract() {
    this.loadingDownload = true;
    this.service.downloadContractPDF(this.contractId, this.contract.language)
      .subscribe(
        res => {
          autoDownloadFile(res, PDF);
          this.loadingDownload = false;
        },
        error => {
          this.loadingDownload = false;
        }
      );
  }

  previewDocument(blob) {
    const fileObjectURL = URL.createObjectURL(blob);
    window.open(fileObjectURL, '_blank');
  }

  openArchiveModal(content) {
    this.modalService.open(content, {backdrop: 'static'});
  }
  cancelContract() {
    this.service.cancelContract(this.contractId)
      .subscribe(
        res => {
          this.toastrService.success(this.translate.instant('You have successfully canceled contract'));
          this.router.navigate([`contracts/`]);
          this.getContractItem(true);
        },
        error => {
          console.error(error);
        }
      );
    this.modalService.dismissAll();
  }

  archiveContract() {
    this.service.archiveContract('/contracts', this.contractId)
      .subscribe(
        res => {
          this.toastrService.success(this.translate.instant('You have successfully deleted contract'));
          this.router.navigate([`contracts/`]);
          this.getContractItem(true);
        },
        error=> {
          console.error(error);
        });
    this.modalService.dismissAll();
  }

  onToggleUsers() {
    const toggle = this.isUsersShown;
    this.isUsersShown = !toggle;
  }
  onCloseUsers() {
    this.isUsersShown = false;
  }
  capitalizeFullName(fullName) {
    return capitalizeFullName(fullName);
  }
  getOwnerStatus() {
    if (this.contract && this.contract.state === ContractStates.CANCELED) {
      return UserStatuses.REJECTED;
    }
    return this.contractState === this.states.DRAFT ? UserStatuses.NOT_SIGNED : UserStatuses.SIGNED;
  }

  onClickPay() {
    this.payLoading = true;
    this.service.onPay(this.contractId).subscribe(res => {
      this.payLoading = false;
      this.toastrService.success(this.translate.instant('Messages.Payment success'));
      this.getContractItem();
    }, error => {
      this.payLoading = false;
      this.toastrService.success(this.translate.instant('Errors.Payment error'));
    });
  }

  getGroupTitle(field) {
    const locale = {
      kk: 'groupTitleKz',
      en: 'groupTitleEn',
      ru: 'groupTitleRu',
    };
    const key = locale[this.translate.currentLang];
    return field[key];
  }

  showField(field) {
    const target = field.targetFields;
    if (field.source && target) {
      if (target.length === 1 && target[0] === '') {
        return true;
      } else if (field.source === 'number') { // TODO: hard code for showing loan amount
        return true;
      } else if (target.length > 0) {
        return false;
      }
    }
    return this.documentFields[field.code] || this.documentFields[field.code] === '';
  }

  goBack() {
    const backUrl = this.route.snapshot.queryParams.backUrl;
    if (backUrl) {
      this.router.navigate([backUrl]);
    } else {
      this.location.back();
    }
  }
  openUsersModal() {
    const userModal = this.modalService.open(UsersComponent, {backdrop: 'static', centered: true});
    userModal.componentInstance.contractId = this.contractId;
    userModal.componentInstance.afterAddUser.subscribe(() => {
      this.getData();
    });
  }
  openRemoveUserModal(content) {
    this.modalService.open(content);
  }
  onRemoveUser(userIin) {
    this.modalService.dismissAll();
    const data = {parties: [{ iin: userIin }]};
    this.userService.deleteUser('/contracts', this.contractId, '/parties', data)
      .subscribe(res => {
        this.getData();
      });
  }
  openTemplate(document) {
    if (document.loading) {
      return;
    }
    document.loading = true;
    this.service.getTemplate({templateId: document.templateId, locale: this.translate.currentLang})
      .subscribe(res => {
        document.loading = false;
        autoDownloadFile(res, PDF);
      });
  }

  getUserType() {
    return (this.initiator && this.auth.user && this.initiator.iin === this.auth.user.iin || this.mode === 'create') ? 'Initiator' : 'Participant';
  }

  canManageUsers() {
    return this.userPermissions.owner && this.contract && this.contract.state === this.states.DRAFT;
  }
}
