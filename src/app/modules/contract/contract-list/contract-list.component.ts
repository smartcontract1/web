import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {LangChangeEvent, TranslateService} from '@ngx-translate/core';
import {Location} from '@angular/common';

import {ContractService} from '../../../_services/contract.service';
import {AuthService} from '../../../_services/auth.service';
import {autoDownloadFile, capitalize, capitalizeFullName, PDF} from '../../share/utils/constants';
import {
  ContractStates,
  ContractStatesList,
  ContractTypes,
  IContract,
  OutboxStates
} from '../../../types/contract.types';
import {NCAService} from '../../../_services/nca.service';
import {ToastrService} from 'ngx-toastr';

export interface IContractsFilter {
  type: ContractTypes;
  start: number;
  limit: number;
  desc: boolean;
  state: ContractStates;
}
export interface IQuery {
  party: string;
  state: ContractStates;
}
@Component({
  selector: 'app-contract-list',
  templateUrl: './contract-list.component.html',
  styleUrls: ['./contract-list.component.css'],
})
export class ContractListComponent implements OnInit, OnDestroy {
  @ViewChild('filterDrop', { static: false }) filterDrop;
  public contractList: IContract[] = [];
  public filter: IContractsFilter = {
    type: ContractTypes.outbox,
    start: 0,
    desc: true,
    limit: 20,
    state: ContractStates.ALL,
  };
  public filterStatuses = OutboxStates;
  public currentLanguage = 'ru';
  public contractsLoading = false;
  public contractsOver = false;
  public certificateChosenSubs;
  public loadingDownload = {};
  public paramsSubs;
  private code: string;
  constructor(
    public service: ContractService,
    private authService: AuthService,
    private router: Router,
    private location: Location,
    private route: ActivatedRoute,
    public translate: TranslateService,
    public ncaService: NCAService,
    public toastr: ToastrService
  ) {
    this.currentLanguage = this.translate.currentLang;
    translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.currentLanguage = event.lang;
    });
  }
  ngOnInit() {
    this.paramsSubs = this.route.queryParams.subscribe((query: IQuery) => {
      this.filter.start = 0;
      this.filter.type = query.party === 'true' ? ContractTypes.inbox : ContractTypes.outbox;
      this.filter.state = query.state || ContractStates.ALL;
      this.filterStatuses = ContractStatesList[this.filter.type];
      this.getContractList(true);
    });
    // When nca certificate chosen:
    this.certificateChosenSubs = this.ncaService.certificateChosen.subscribe(val => {
      if (val) {
        this.getContractList();
      }
    });
  }
  getContractList(update = false): void {
    this.getContracts(update);
  }
  ngOnDestroy(): void {
    this.paramsSubs.unsubscribe();
    this.certificateChosenSubs.unsubscribe();
  }
  onScroll() {
    if (!this.contractsLoading && !this.contractsOver) {
      this.getContracts();
    }
  }

  getContracts(update = false) {
    if (this.authService.isLoggedIn && !this.authService.user) {
      this.authService.getProfile().subscribe(res => {
        this.getContracts(update);
      });
      return;
    }
    this.contractsLoading = true;
    if (update) {
      this.contractList = [];
    }
    this.service.getContractList(this.filter.type, this.filter).subscribe(
      (res: any) => {
        const all: any[] = this.filter.type === ContractTypes.inbox ? this.filterInbox(res) : res;
        this.contractList = [...this.contractList, ...all];
        this.filter.start += this.filter.limit;
        this.contractsOver = all.length < this.filter.limit;
        this.contractsLoading = false;
        this.contractList.forEach((c: IContract) => { // TODO: optimize this request, it is used to update contract state by getting contract item
          c.isRead = c.extras.readByParties.some((r) => r === this.authService.user.iin);
          this.service.getContractItem(c.contractId).subscribe((contract: IContract) => {
            c.parties = contract.parties;
            c.partiesFullNames = contract.partiesFullNames;
            c.language = contract.language;
          });
        });
      },
      error => {
        console.error(error);
        this.contractsLoading = false;
      }
    );
  }
  filterInbox(all) {
    return all.filter(c => c.state !== ContractStates.DRAFT || c.state !== ContractStates.ARCHIVED);
  }

  onDownload(contract: IContract) {
    this.loadingDownload[contract.contractId] = true;
    if (contract.state !== ContractStates.DRAFT) {
      this.downloadContract(contract);
    } else {
      this.previewContract(contract);
    }
  }
  previewContract(contract: IContract) {
    const data = {
      contractId: contract.contractId,
      locale: contract.language
    };
    this.service.previewContract('/documents/generator/previewPDF', data)
      .subscribe(
        res => {
          this.loadingDownload[contract.contractId] = false;
          this.previewDocument(res);
        }, () => {
          this.loadingDownload[contract.contractId] = false;
        }
      );
  }

  downloadContract(contract: IContract) {
    this.service.downloadContractPDF(contract.contractId, contract.language)
      .subscribe(
        res => {
          autoDownloadFile(res, PDF);
          this.loadingDownload[contract.contractId] = false;
        },
        () => {
          this.loadingDownload[contract.contractId] = false;
        }
      );
  }

  previewDocument(blob) {
    const fileObjectURL = URL.createObjectURL(blob);
    window.open(fileObjectURL, '_blank');
  }

  getParticipants(item) {
    if (!item.parties) {
      return '';
    }
    const firstParticipant = {
      ...item.parties[0]
    };
    Object.assign(firstParticipant, item.partiesFullNames[firstParticipant.iin]);
    item.firstParticipant = firstParticipant || '';

    if (this.filter.type === ContractTypes.outbox) {
      if (item.parties.length === 0) {
        return this.translate.instant('The list of participants is empty');
      }
      const first = item.firstParticipant;
      const one = `${this.translate.instant('For')}: <span>${capitalize(first.last + ' ' + first.first)}</span>`;
      const several = item.parties.length > 1 ? `<span> ${this.translate.instant('And')} ${item.parties.length-1}` : '';
      return one + several;
    }
    return `${this.translate.instant('From')}: <span class="text-capitalize">${capitalizeFullName(item.initiatorFullName)}</span>`;
  }
}
