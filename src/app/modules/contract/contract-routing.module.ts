import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ContractItemComponent} from './contract-item/contract-item.component';
import {ContractListComponent} from './contract-list/contract-list.component';
import {ContractCreateListComponent} from './list/list.component';
import {HelpComponent} from './help/help.component';

const routes: Routes = [
  {
    path: '',
    component: ContractListComponent,
  },
  {
    path: 'create/:documentId',
    component: ContractItemComponent,
    data: {mode: 'create'}
  },
  {
    path: 'create',
    component: ContractCreateListComponent
  },
  {
    path: 'help',
    component: HelpComponent
  },
  {
    path: 'edit/:documentId/:contractId',
    component: ContractItemComponent,
    data: {mode: 'edit'}
  },
  {
    path: 'read/:documentId/:contractId',
    component: ContractItemComponent,
    data: {mode: 'read'}
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContractRoutingModule { }
