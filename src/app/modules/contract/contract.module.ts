import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {NgbDatepickerModule, NgbDropdownModule} from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';

import { ContractRoutingModule } from './contract-routing.module';
import { ShareModule } from '../share/share.module';

import { ItemComponent } from './item/item.component';
import { ContractItemComponent } from './contract-item/contract-item.component';
import { UsersComponent } from './users/users.component';
import {FilterComponent} from './filter/filter.component';
import {ContractCreateFormComponent} from './form/form.component';
import {ContractCreateListComponent} from './list/list.component';
import {RequestModule} from '../request/request.module';
import { ContractListComponent } from './contract-list/contract-list.component';
import { HelpComponent } from './help/help.component';
import {AddressSelectComponent} from './address-select/address-select.component';
import {NgxMaskModule} from "ngx-mask";

@NgModule({
  declarations: [
    ItemComponent,
    ContractItemComponent,
    UsersComponent,
    FilterComponent,
    ContractCreateFormComponent,
    ContractCreateListComponent,
    ContractListComponent,
    HelpComponent,
    AddressSelectComponent
  ],
  imports: [
    CommonModule,
    ContractRoutingModule,
    FormsModule,
    NgbDatepickerModule,
    ShareModule,
    NgbDropdownModule,
    TranslateModule,
    RequestModule,
    NgxMaskModule.forChild(),
  ],
  providers: [
    FormsModule,
  ],
  entryComponents: [
    UsersComponent,
    AddressSelectComponent
  ]
})
export class ContractModule { }
