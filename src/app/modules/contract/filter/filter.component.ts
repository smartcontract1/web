import {Component, Input, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

import {ContractStates} from '../../../types/contract.types';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {
  @Input() activeState;
  @Input() states;
  @Input() contractsAreLoading;
  public active;

  constructor(
    public translate: TranslateService
  ) {}

  ngOnInit(): void {
    this.active = this.activeState || this.states[0];
  }

  navigate(filter) {
    return !this.contractsAreLoading ? {state: filter.state} : null;
}
}
