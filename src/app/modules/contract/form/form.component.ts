import {Component, OnDestroy, OnInit, Input, OnChanges, SimpleChanges} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NgbCalendar, NgbDate, NgbDateStruct, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {LangChangeEvent, TranslateService} from '@ngx-translate/core';
import {Location} from '@angular/common';

import {ContractService} from '../../../_services/contract.service';
import {AuthService} from '../../../_services/auth.service';
import {isAuto, isDocumentWithPayment, transformDate} from '../../share/utils/constants';
import * as moment from 'moment';
import {IContract, IDocument, IField, IFieldSourceType, IFieldValue, IValidation} from '../../../types/contract.types';
import {RequestModalComponent} from '../../request/request-modal/request-modal.component';
import {AddressSelectComponent} from '../address-select/address-select.component';

@Component({
  selector: 'app-create-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class ContractCreateFormComponent implements OnInit, OnDestroy, OnChanges {
  @Input() documentId; // or template id
  @Input() contractId;
  @Input() mode: 'create' | 'edit' | 'read';
  @Input() currentDocument: IDocument;
  @Input() currentContract: IContract;
  public fieldObjects: IField[] = []; // fields list
  public fieldsValue: IFieldValue = {}; // current field values

  public fromDate: NgbDate; // ?
  public toDate: NgbDate; // ?
  public staticSources: any = {};
  public dynamicListSources: any = {};
  public currentLanguage = 'ru';
  public formValidated = false;
  public formInputsValidated = false;
  public loading = false;

  public modelDetails: any = {
    inputForm: {fields: {}}
  };
  public validation: IValidation = {};
  private langSubscription;
  public submitLoading = false;
  public sourceTypes = IFieldSourceType;
  public defaultMessages = {};

  constructor(
    private modalService: NgbModal,
    private service: ContractService,
    private route: ActivatedRoute,
    private router: Router,
    calendar: NgbCalendar,
    public authService: AuthService,
    public translate: TranslateService,
    private location: Location
  ) {
    this.fromDate = calendar.getToday();
    this.toDate = calendar.getNext(calendar.getToday(), 'd', 10);
    // language
    this.currentLanguage = this.translate.currentLang;
    this.langSubscription = translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.currentLanguage = event.lang;
    });
  }
  ngOnInit(): void {}
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.currentDocument) {
      this.currentDocument = changes.currentDocument.currentValue;
      this.fieldObjects = this.currentDocument.fields;
      if (this.mode === 'create') {
        this.organizeFields();
      }
    }
    if (changes.currentContract) {
      this.currentContract = changes.currentContract.currentValue;
      this.getContractItem();
    }
  }
  ngOnDestroy(): void {
    this.langSubscription.unsubscribe();
  }

  getContractItem() {
    if (!this.contractId || !this.currentDocument) {
      return;
    }
    this.organizeFields(this.currentContract.fields);
  }
  getDictionary(field) {
    if(field.source === 'estates') {
      this.service.get(`/dictionary/${field.source}/${this.authService.user.iin}/`)
        .subscribe(res => {
          this.staticSources[field.source] = res;
        });
    } else {
      this.service.get(`/dictionary/${field.source}`)
        .subscribe(res => {
          this.staticSources[field.source] = res;
          const elements = this.staticSources[field.source].elements;
          if (field.dataType === 'INTEGER') {
            this.staticSources[field.source].elements = elements.sort((a, b) => a.id - b.id);
          }
        });
    }
  }
  organizeFields(fields?: any) {
    this.fieldObjects.forEach((field: IField) => {
      if (field.sourceType && field.sourceType === 'STATIC') {
        this.getDictionary(field);
      }
      // Set default values for model
      this.fieldsValue[field.code] = field.defaultValue;
      this.validation[field.code] = {
        isValid: false,
        startValidation: false,
        dynamicValid: true
      };
      // Default validator for double:
      if (field.dataType === 'DOUBLE' && field.validator === '' && !field.validator) {
        field.validator = '^(0|([1-9][0-9]*))(\\.[0-9]+)?$';
      }
      const today: NgbDateStruct = {
        day: moment().date(),
        month: moment().month() + 1,
        year: moment().year(),
      };
      // Check for current date:
      if (field.dateGreaterThan === 'current_date') {
        field.minDate = today;
      }
      if (field.dateLessThan === 'current_date') {
        field.maxDate = today;
      }
      // Get own fields
      if (field.code && field.code.includes('iin_own') && field.source && !field.code.includes('__')) {
        field.readOnly = true;
        const iin = (fields && fields[field.code]) ? fields[field.code] : this.authService.user.iin;
        this.fieldsValue[field.code] = iin;
        this.processDynamicSource(field, iin);
      }
    });
    this.loading = false;
    if (fields) {
      this.fillContractFields(fields);
    }
  }
  fillContractFields(fields) {
    Object.keys(fields).map((code: string) => {
      const field = this.getFieldByCode(code);
      if (this.fieldsValue[code] !== undefined) {
        this.fieldsValue[code] = fields[code];
      }
      // Dynamic sources:
      if (field && field.source && field.sourceType === IFieldSourceType.DYNAMIC && !field.code.includes('iin_own') && field.source !== 'person' && this.currentContract.initiator.iin === this.authService.user.iin) {
        this.processDynamicSource(field, this.fieldsValue[field.code]);
      }
    });
    this.organizeDateFields(fields);
  }
  organizeDateFields(fields) {
    Object.keys(fields).map((code: string) => {
      const field = this.getFieldByCode(code);
      if (field && field.dataType === 'LOCAL_DATE') {
        const date = this.fieldsValue[field.code];
        const event = {
          date: {
            day: moment(date, ['DD.MM.YYYY']).date(),
            month: moment(date, ['DD.MM.YYYY']).month() + 1,
            year: moment(date, ['DD.MM.YYYY']).year(),
          },
          val: date
        };
        this.onDateChange(event, field);
      }
    });
  }
  disableNext() {
    return this.submitLoading || this.fieldObjects.find((f: IField) => f.loading);
  }
  next() {
    console.log(this.fieldsValue);
    this.submitLoading = true;
    if (!this.validateForm()) {
      this.submitLoading = false;
      return;
    }
    this.mode === 'create' ? this.createContract() : this.updateContract();
  }

  createContract() {
    const document = {
      documentId: this.documentId,
      initiator: {iin: this.authService.user.iin},
      inputForm: {fields: this.fieldsValue},
      language: this.translate.currentLang
    };
    this.service.createContract(document).subscribe((res: any) => {
      const contractId = res.id;
      this.createWorkflow(contractId);
      this.submitLoading = false;
      this.router.navigate([`/contracts/read/${this.documentId}/${contractId}`], {queryParams: {backUrl: '/contracts'}});
    }, error => {
      this.submitLoading = false;
    });
  }

  createWorkflow(contractId) {
    if (!isDocumentWithPayment(this.documentId)) {
      return;
    }
    const data = {
      workflow_type: isAuto(this.documentId) ? 'reg_01_workflow' : 'pledge_workflow',
      contract_id: contractId
    };
    this.service.postWorkflow(data)
      .subscribe((res: any) => {
        console.log(res);
      });
  }

  updateContract() {
    const editDocument = {inputForm: {fields: this.fieldsValue}};
    this.service.updateContract(this.contractId, editDocument ).subscribe(() => {
      this.submitLoading = false;
      this.router.navigate([`/contracts/read/${this.documentId}/${this.contractId}`]);
    }, error => {
      this.submitLoading = false;
    });
  }

  onDateChange(event, field) {
    // Process max and min dates with ralated fields:
    this.modelDetails.inputForm.fields[field.code] = event.date;
    this.fieldsValue[field.code] = event.val;
    const relatedLess = this.fieldObjects.find(f => f.dateLessThan === field.code);
    if (relatedLess) {
      relatedLess.maxDate = event.date;
    }
    const relatedGreater = this.fieldObjects.find(f => f.dateGreaterThan === field.code);
    if (relatedGreater) {
      relatedGreater.minDate = event.date;
    }
    this.validateField(field);
  }

  onDateIntervalChange(event, field) {
    this.fieldsValue[field.code] = event.val;
    this.validateField(field);
  }

  onTimeChange(event, field) {
    this.fieldsValue[field.code] = event.val;
    this.modelDetails.inputForm.fields[field.code] = event.time;
    this.validateField(field);
  }

  onDateTimeChange(event, field) {
    this.fieldsValue[field.code] = event.val;
    this.validateField(field);
  }

  changePhone(event, field) {
    this.fieldsValue[field.code] = event;
    this.validateField(field);
  }

  staticSourceSelected(field, list, model) {
    for (const item of list) {
      if (item.id === model && field.targetFields) {
        for (const targetField of field.targetFields) {
          this.fieldsValue[targetField] = item.name[this.currentLanguage];
        }
      }
    }
    // TODO: remove hard code, api should add new type of Source, like STATIC_2
    const getSource = (source: string) => {
      switch (source) {
        case 'cities':
        case 'kato':
        case 'banks':
        case 'variant_payment':
        case 'kbe':
        case 'rental_period':
        case 'number_of_months':
          return false;
        default:
          return true;
      }
    };
    if (getSource(field.source)) {
      const sourceField = {
        source: field.source === 'kato' ? `${field.source}/${model}` : `${field.source}_${model}`,
        sourceType: 'DYNAMIC',
        code: field.code,
        avoidCheck: true
      };
      this.processDynamicSource(sourceField, '');
    } else if (field.source === 'banks') {
      const val = this.fieldsValue[field.code];
      const url = `/dictionary/banks/${val}`;
      this.getSource(field, val, url);
    }
    this.validateField(field);
  }

  processDynamicSource(field, event) {
    const val = String(event.target ? event.target.value : event);
    if (((field.validator && val.match(field.validator)) || !field.validator) && ((field.length && (field.additional && val.length <= field.length) || val.length === field.length) || !field.length)) {
      // if entered value passed validation OR there is not a validator
      if (field.sourceType && field.sourceType === 'DYNAMIC') {
        field.source.split(',').map(source => {
          field.sourceValidation = {};
          field.sourceValidation[source] = {
            valid: true,
            message: {}
          };
          this.getDynamicSource(field, val, source);
        });
      }
    } else {
      this.validation[field.code].dynamicValid = false;
      this.validateField(field);
    }
  }
  getDynamicSource(field, val, source) {
    field.loading = true;
    const url = `/dictionary/${source}/${val ? val+'/' : ''}`;
    this.emptyDynamicFields(field.code);
    this.getSource(field, val, url, source);
  }

  processDynamicListSource(field, event) {
    const iin = this.dynamicListSources[field.code].source;
    const rk = event.target.value;
    this.emptyDynamicFields(field.code);
    if (rk === '') {
      return;
    }
    if (field.source === 'autoInfo') {
      this.getSource(field, '', `/dictionary/autoInfo/${rk}`);
    } else {
      // when source is estates:
      this.getSource(field, this.fieldsValue[field.code], `/dictionary/estatesFull/${iin}/${rk}`);
    }
  }

  getSource(field, val, url, source?) {
    const params = {
      doc_id: this.documentId,
      ...this.contractId && {contractId: this.contractId}
    };
    const header = {
      initiator: this.authService.user.iin
    };
    this.service.getDictionary(url, header, params)
      .subscribe((result: any) => {
        if (result.elements.length === 0) {// make empty related fields that start prefix is same
          this.emptyDynamicFields(`${field.code}__${source}`);
        }
        // prepare dynamic lists
        if (result.id === 'kato') {
          this.dynamicListSources[`kato_name`] = {
            elements: result.elements,
            source: val
          };
        }
        if (result.dictionaryType === IFieldSourceType.DYNAMIC_LIST) {
          const fieldCode = `${field.code}__${result.id}`;
          this.dynamicListSources[fieldCode] = {
            elements: result.elements,
            source: val
          };
        }
        // If field has targetFields, ex. for iin
        if (field.targetFields && field.targetFields.length > 0) {
          for (const code of field.targetFields) {
            for (const elem of result.elements) {
              if (elem.id === val) {
                this.fieldsValue[code] = elem.name[this.currentLanguage];
                const currentField = this.getFieldByCode(code);
                this.validateField(currentField);
              }
            }
          }
        }
        if (result.elements) {
          // if has not target fields, ex, bin
          result.elements.map( elem => {
            const fieldCode = field.code + elem.id;
            // Assign field value if it is exists:
            if (this.fieldsValue[fieldCode] !== undefined) {
              this.fieldsValue[fieldCode] = elem.name[this.currentLanguage];
              const currentField = this.getFieldByCode(fieldCode);
              if (currentField && currentField.dataType === 'LOCAL_DATE') {
                this.fieldsValue[fieldCode] = transformDate(this.fieldsValue[fieldCode]);
              }
              this.validateField(currentField);
            }
          });
        }
        // validation
        if (source !== 'estates') {
          this.validation[field.code].startValidation = true;
          this.validation[field.code].isValid = true;
          this.validation[field.code].dynamicValid  = true;
        }
        this.validation[field.code].showAgreementLink = false;
        field.loading = false;
      }, error => {
        console.error(error);
        this.defaultMessages[field.code] = field.errorMessage;
        if (error.status === 400 && error.error.developerMessage === 'PERMISSION_DENIED') {
          const text = 'Errors.To add a participant, you must obtain his consent to the collection and processing of personal data';
          this.validation[field.code].showAgreementLink = true;
          // TODO: add appropriate translations:
          const message = {
            kk: this.translate.instant(text),
            ru: this.translate.instant(text),
            en: this.translate.instant(text),
          };
          field.errorMessage = message;
          if (field.sourceValidation) {
            field.sourceValidation[source] = {
              valid: false,
              message
            };
          }
        } else {
          field.errorMessage = this.defaultMessages[field.code];
        }
        field.loading = false;
        // validation
        if (!field.avoidCheck && this.mode !== 'read') {
          this.validation[field.code].startValidation = true;
          this.validation[field.code].isValid = false;
          this.validation[field.code].dynamicValid  = false;
          this.formValidated = true;
        }
        this.emptyDynamicFields(field.code);
      });
  }
  openAgreementModal(templateId, iin?) {
    const modalRef = this.modalService.open(RequestModalComponent, {scrollable: true, backdrop: 'static'});
    modalRef.componentInstance.templateId = templateId;
    modalRef.componentInstance.iin = iin;
  }
  emptyDynamicFields(code) {
    if (this.mode === 'read') {
      return;
    }
    this.fieldObjects.forEach(f => {
      if (f.code) {
        const field = this.getFieldByCode(f.code);
        if (field && f.code.includes(code) && f.code !== code) {
          this.fieldsValue[f.code] = '';
          this.validateField(field);
        }
      }
    });
  }

  validateLength(event, field) {
    return (!field.length || event.target.value.length < field.length) && ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode === 46 && field.dataType !== 'INTEGER'));
  }

  validateField(field, getDynamicField?) { // validates only current changed field:
    if (!field && this.mode === 'read') {
      return;
    }
    const currentValue = this.fieldsValue[field.code];
    const len = field.length ? field.length >= currentValue.toString().length : true;
    const valid = field.validator ? !!currentValue.toString().match(field.validator) : true;
    const mandatory = field.mandatory && field.display ? currentValue && currentValue.toString().length > 0 : true;
    if (!this.validation[field.code]) {
      this.validation[field.code] = {
        startValidation: false,
      };
    }
    this.validation[field.code].isValid = len && valid && mandatory;
    if (field.sourceType === 'DYNAMIC' && getDynamicField) {
      // When field length enough and field value is valid
      if (field.length && field.length === currentValue.toString().length && this.validation[field.code].isValid) {
        this.processDynamicSource(field, currentValue);
      }
      this.validation[field.code].isValid = this.validation[field.code].dynamicValid && this.validation[field.code].isValid;
      if (!this.validation[field.code].isValid) {
        this.emptyDynamicFields(field.code);
      }
    }
    if (field.sourceType && field.sourceType === 'STATIC') {
      if (field.errorMessage[this.currentLanguage] === '') {
        field.errorMessage[this.currentLanguage] = this.translate.instant('Errors.Not selected');
      }
    }
    this.validation[field.code].startValidation = true;
  }

  validateForm() {
    this.formInputsValidated = true;
    for (const field of this.fieldObjects) {
      this.validateField(field);
      if (!this.validation[field.code].isValid) {
        this.formInputsValidated = false;
      }
    }
    this.formValidated = true;
    return this.formInputsValidated;
  }

  getFieldStatusClass(field, type='input') {
    if (this.mode === 'read') {
      return;
    }
    const validate = this.validation[field.code];
    const value = this.fieldsValue[field.code];
    const classes = {
      'is-loading': field.loading,
      'is-invalid': validate.startValidation && !validate.isValid && !field.loading,
      'is-valid': validate.startValidation && validate.isValid && !field.loading
    };
    if (type === 'select' && value === '' && validate.startValidation) {
      classes['is-invalid'] = true;
      classes['is-valid'] = false;
    }
    if (field.sourceType === IFieldSourceType.DYNAMIC) {
      field.source.split(',').forEach((s: string) => {
        if (field.sourceValidation && field.sourceValidation[s] && field.sourceValidation[s].valid === false) {
          classes['is-invalid'] = true;
          classes['is-valid'] = false;
          field.errorMessage = field.sourceValidation[s].message;
        }
      });
    }
    return  classes;
  }

  getFieldByCode(code) {
    return this.fieldObjects.find(f => f.code === code);
  }

  getGroupTitle(field) {
    const locale = {
      kk: 'groupTitleKz',
      en: 'groupTitleEn',
      ru: 'groupTitleRu',
    };
    const key = locale[this.translate.currentLang];
    return field[key];
  }

  checkForValid(str: string, validator) {
    const regex = new RegExp(validator);
    return regex.test(str);
  }

  keypressValidate(event, field: IField) {
    if (event && field && field.validator) {
      const str = this.fieldsValue[field.code] || '';
      if (field.loading) {
        return false;
      }
      if (event.type !== 'paste') { // Allow paste everything
        return this.checkForValid( str + event.key, field.validator);
      }
    }
    return true;
  }

  // Select for address of contract
  openAddressModal(field: IField, e) {
    e.stopPropagation();
    const modal = this.modalService.open(AddressSelectComponent, {size: 'lg', scrollable: true});
    modal.componentInstance.onSelectFullAddress.subscribe((res) => {
      this.fieldsValue[field.targetFields[0]] = res.name[this.translate.currentLang];
      this.fieldsValue[field.code] = res.id;
      const data = {elements: res};
      this.staticSources[field.source] = data;
    });
  }

  onChangePhone($event: any, field: IField) {
    this.fieldsValue[field.code] = $event.target.value;
  }
}
