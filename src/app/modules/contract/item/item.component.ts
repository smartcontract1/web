import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {LangChangeEvent, TranslateService} from '@ngx-translate/core';

import {ContractService} from '../../../_services/contract.service';
import {AuthService} from '../../../_services/auth.service';
import {NCAService} from '../../../_services/nca.service';
import {
  autoDownloadFile,
  capitalizeFullName,
  CESSION_ID,
  defaultDateTimeFormat, isDocumentWithPayment,
  PDF,
  PLEDGE_ID,
  transformDate
} from '../../share/utils/constants';
import {ContractStates, UserStatuses} from '../../../types/contract.types';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css'],
  providers: [NgbModalConfig, NgbModal]
})
export class ItemComponent implements OnInit, OnDestroy {

  public contract: any;
  public contractId: string;
  public contractUsers = [];
  public contractDocument: any;
  public documentFields;
  public initiator;
  public contractState: string;
  public contractForSign: any;
  public lang = 'ru';
  public isUsersShown = false;
  public loading = false;
  public backUrl = '';
  public currentLanguage = 'ru';
  public ifOpenSignModal = false;
  public userPermissions = {
    owner: false,
    signed: false,
    rejected: false,
  };
  public states = ContractStates;
  public dateTimeFormat = defaultDateTimeFormat;
  public loadingDownload = false;
  public certificateChosenSubs;
  public shouldPay = false;
  public canPay = false;
  public paid = false;
  public rkCode;
  public payLoading = false;
  public waitingConductor = false;
  private ncaLoading: boolean;

  constructor(
    config: NgbModalConfig,
    public service: ContractService,
    private router: Router,
    private route: ActivatedRoute,
    public auth: AuthService,
    public nca: NCAService,
    private modalService: NgbModal,
    public translate: TranslateService,
    private toastrService: ToastrService,
    private location: Location
  ) {
    config.backdrop = 'static';
    this.currentLanguage = this.translate.currentLang;
    translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.currentLanguage = event.lang;
    });
    this.certificateChosenSubs = this.nca.certificateChosen.subscribe(val => {
      if (val) {
        this.ifOpenSignModal = false;
      }
      this.getContractItem(true);
    });
  }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.contractId = params.id;
      this.backUrl = this.router.url.replace(this.contractId, '');
      this.getContractItem();
      this.closeSignModal();
      this.onCloseUsers();
    });
  }
  ngOnDestroy(): void {
    if (this.certificateChosenSubs) {
      this.certificateChosenSubs.unsubscribe();
    }
  }

  getContractItem(updated = false) {
    this.loading = true;
    this.service.getContractItem(this.contractId).subscribe(res => {
      this.loading = false;
      this.parseContract(res);
      this.getWorkflow();
      this.getContractDocument(this.contract.documentId);
      if (updated) {
        this.service.addToUpdatedContracts(this.contract);
      }
    });
  }
  getWorkflow() {
    this.rkCode = undefined;
    this.canPay = this.shouldPay = this.paid = false;
    if (isDocumentWithPayment(this.contract.documentId)) {
      this.service.getWorkflowState(this.contractId).subscribe((res: any) => {
        if (!res.data) {
          return;
        }
        const {tasks} = res.data;
        const rk = tasks.find((t: any) => t.taskType === 'get_rk_code');
        this.waitingConductor = rk && rk.status === 'IN_PROGRESS';
        const payment = tasks.find((t: any) => t.taskType === 'wait_for_mu_payment');
        this.canPay = payment && payment.status === 'IN_PROGRESS';
        this.paid = payment && payment.status === 'COMPLETED';
        this.shouldPay = this.canPay;
        if (this.documentFields.iin_pledgor) { // if pledge contract
          this.shouldPay = this.canPay && (this.documentFields.iin_pledgor === this.auth.user.iin);
        }
        this.rkCode = rk && !(payment && payment.status === 'COMPLETED') ? rk.outputData.rk_code : undefined;
      },
      error => {
        console.error(error);
      });
    }
  }
  convertFromExpToDecimal(key, value) {
    if (value.match('[-+]?[0-9]*\\.?[0-9]+([E][0-9]+)')) {
        const indexOfExp = value.indexOf('E');
        const result = value.slice(0, indexOfExp) * Math.pow(10, value.slice(indexOfExp + 1));
        this.contract.inputForm.fields[key] = result.toLocaleString().replace(/\s/g,'');
    }
  }
  parseContract(res: any) {
    this.contract = res;
    this.currentLanguage = this.contract.language || this.currentLanguage;
    for (const key in this.contract.inputForm.fields) {
      this.convertFromExpToDecimal(key, this.contract.inputForm.fields[key]);
    }
    this.documentFields = this.contract.inputForm.fields;
    this.contractState = this.contract.state;
    this.initiator = {
      iin: this.contract.initiator.iin,
      fullName: this.contract.initiatorFullName
    };
    this.contractUsers = this.contract.parties.map(p => {
      const user = {
        iin: '',
        fullName: {},
        status: ''
      };
      user.iin = p.iin;
      user.fullName = this.contract.partiesFullNames[user.iin];
      user.status = UserStatuses.NOT_SIGNED;
      this.contract.confirmedParties.forEach( item => {
        if (item.iin === user.iin) {
          user.status = UserStatuses.SIGNED;
        }
      });
      this.contract.rejectedParties.forEach( item => {
        if (item.party.iin === user.iin) {
          user.status = UserStatuses.REJECTED;
        }
        if (item.party.iin === this.auth.user.iin) {
          this.userPermissions.rejected = true;
        }
      });
      return user;
    });
    this.setUserPermissions();
  }

  setUserPermissions() {
    this.userPermissions.owner = this.initiator.iin === this.auth.user.iin;
    this.userPermissions.signed = this.userPermissions.owner ?
      this.contract.initiatorSignatureProvided :
      !!this.contract.confirmedParties.find(p => p.iin.includes(this.auth.user.iin));
  }

  getContractDocument(docId) {
    this.service.getContractDocument(docId)
      .subscribe(res => {
        this.contractDocument = res;
        this.contractDocument.inputForm.fields[1].sort((obj1, obj2) => obj1.order - obj2.order);
        this.contractDocument.inputForm.fields[1].forEach( field => {
          if (field.dataType === 'LOCAL_DATE') {
            this.documentFields[field.code] = transformDate(this.documentFields[field.code]);
          }
        });
      });
  }

  checkSignContract() {
    return (this.contractState === 'DRAFT' && this.contract.initiatorCanSign) ||
      (!this.userPermissions.owner && this.contract.partiesCanSign);
  }
  checkRejectContract() {
    return !this.userPermissions.owner && !this.userPermissions.signed && !this.userPermissions.rejected;
  }
  checkEditContract() {
    return this.userPermissions.owner && this.contractState === 'DRAFT';
  }
  checkManageUsers() {
    return this.userPermissions.owner && !this.userPermissions.signed;
  }
  openSignModal(content) {
    this.ifOpenSignModal = true;
    this.service.getContractForSign('/contracts', this.contractId)
      .subscribe(res => {
        this.contractForSign = res;
        this.nca.xmlToSign = res;
      });
  }

  closeSignModal() {
    this.ifOpenSignModal = false;
  }

  chooseCertificate() {
    this.ncaLoading = true;
    setTimeout(() => {
      this.ncaLoading = false;
    },2500);
    this.nca.selectSignType('SIGN', this.contractId, this.userPermissions.owner);
  }

  signContract() {
    const data = {
      initiatorSignature: {
        signature: this.nca.certificate,
        publicKey: ''
      }
    };
    this.service.signContract(this.contractId, this.userPermissions.owner, data)
      .subscribe(
        res => {
          this.ifOpenSignModal = false;
          this.getContractItem();
        },
        error => {
          console.error(error);
        }
      );
  }

  openRejectModal(content) {
    this.modalService.open(content);
  }

  onRejectSignContract() {
    const data = { parties:
        [{
          party: {
            iin: this.auth.user.iin
          },
          reason: 'Reject with clicking Reject Button'
        }]};
    this.service.rejectSignContract(this.contractId, data)
      .subscribe(
        res => {
          this.getContractItem(true);
        },
        error => {
          this.toastrService.error(`Errors.${this.translate.instant(error)}`);
        }
      );
    this.modalService.dismissAll();
  }

  onEditContract() {
    this.router.navigate([`/contracts/edit/${this.contractId}`])
      .then(
        res => {},
        reason => {
          console.error(reason);
        }
      );
  }

  onDownloadContract() {
    (this.contractState !== 'DRAFT') ? this.downloadContract() : this.previewContract();
  }

  previewContract() {
    this.loadingDownload = true;
    const data = {
      contractId: this.contractId,
      locale: this.contract.language
    };
    this.service.previewContract('/documents/generator/previewPDF', data)
      .subscribe(
        res => {
          this.loadingDownload = false;
          this.previewDocument(res);
        }, error => {
          this.loadingDownload = false;
        }
      );
  }

  downloadContract() {
    this.loadingDownload = true;
    this.service.downloadContractPDF(this.contractId, this.contract.language)
      .subscribe(
        res => {
          autoDownloadFile(res, PDF);
          this.loadingDownload = false;
        },
        error => {
          this.loadingDownload = false;
        }
      );
  }

  previewDocument(blob) {
    const fileObjectURL = URL.createObjectURL(blob);
    window.open(fileObjectURL, '_blank');
  }

  openArchiveModal(content) {
    this.modalService.open(content);
  }
  cancelContract() {
    this.service.cancelContract(this.contractId)
      .subscribe(
        res => {
          this.toastrService.success(this.translate.instant('You have successfully canceled contract'));
          this.router.navigate([`contracts/`]);
          this.getContractItem(true);
        },
        error => {
          console.error(error);
        }
        );
    this.modalService.dismissAll();
  }

  archiveContract() {
    this.service.archiveContract('/contracts', this.contractId)
      .subscribe(
        res => {
          this.toastrService.success(this.translate.instant('You have successfully deleted contract'));
          this.router.navigate([`contracts/`]);
          this.getContractItem(true);
        },
      error=> {
          console.error(error);
        });
    this.modalService.dismissAll();
  }

  onToggleUsers() {
    const toggle = this.isUsersShown;
    this.isUsersShown = !toggle;
  }
  onCloseUsers() {
    this.isUsersShown = false;
  }
  capitalizeFullName(fullName) {
    return capitalizeFullName(fullName);
  }
  getOwnerStatus() {
    if (this.contract.state === ContractStates.CANCELED) {
      return UserStatuses.REJECTED;
    }
    return this.contractState === this.states.DRAFT ? UserStatuses.NOT_SIGNED : UserStatuses.SIGNED;
  }

  onClickPay() {
    this.payLoading = true;
    this.service.onPay(this.contractId).subscribe(res => {
      this.payLoading = false;
      this.toastrService.success(this.translate.instant('Messages.Payment success'));
      this.getContractItem();
    }, error => {
      this.payLoading = false;
      this.toastrService.success(this.translate.instant('Errors.Payment error'));
    });
  }

  getGroupTitle(field) {
    const locale = {
      kk: 'groupTitleKz',
      en: 'groupTitleEn',
      ru: 'groupTitleRu',
    };
    const key = locale[this.translate.currentLang];
    return field[key];
  }

  showField(field) {
    const target = field.targetFields;
    if (field.source && target) {
      if (target.length === 1 && target[0] === '') {
        return true;
      } else if (field.source === 'number') { // TODO: hard code for showing loan amount
        return true;
      } else if (target.length > 0) {
        return false;
      }
    }
    return this.documentFields[field.code] || this.documentFields[field.code] === '';
  }

  goBack() {
    this.location.back();
  }
}
