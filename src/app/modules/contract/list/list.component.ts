import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Router} from '@angular/router';
import {ContractService} from '../../../_services/contract.service';
import {autoDownloadFile, PDF} from '../../share/utils/constants';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {RequestModalComponent} from '../../request/request-modal/request-modal.component';
import {IDocument} from '../../../types/contract.types';
import {AuthService} from '../../../_services/auth.service';

@Component({
  selector: 'app-create-list-form',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ContractCreateListComponent implements OnInit {

  public documents: IDocument[] = [];
  public currentLanguage = 'ru';
  public documentsLoading: boolean;

  constructor(
    private service: ContractService,
    private router: Router,
    public translate: TranslateService,
    private modalService: NgbModal,
    private authService: AuthService
  ) {
    this.currentLanguage = this.translate.currentLang;
    this.getDocuments();
  }

  ngOnInit() {}

  openTemplate(document) {
    if (document.loading) {
      return;
    }
    document.loading = true;
    this.service.getTemplate({templateId: document.templateId, locale: this.translate.currentLang})
      .subscribe(res => {
        document.loading = false;
        autoDownloadFile(res, PDF);
      });
  }

  openModal(templateId) {
    const modalRef = this.modalService.open(RequestModalComponent, {scrollable: true, backdrop: 'static'});
    modalRef.componentInstance.templateId = templateId;
  }

  getDocuments() {
    if (this.authService.isLoggedIn && !this.authService.user) {
      this.documentsLoading = true;
      this.authService.getProfile().subscribe(res => {
        this.getDocuments();
      });
      return;
    }
    this.documentsLoading = true;
    this.service.get('/documents/read?start=0&limit=10&state=ACTIVE')
      .subscribe((res: IDocument[]) => {
        this.documents = res;
        this.documentsLoading = false;
        this.documents.forEach(el => {
          el.loading = true;
          this.service.get(`/documents/${el.documentId}`).subscribe((r: any) => {
            el.templateId = r.templateId;
            el.loading = false;
          });
        });
      });
  }
}
