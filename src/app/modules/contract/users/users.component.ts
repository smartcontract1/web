import {Component, Input, Output, OnInit, EventEmitter} from '@angular/core';
import {UserService} from '../../../_services/user.service';
import {ActivatedRoute} from '@angular/router';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})

export class UsersComponent implements OnInit {
  public iinText = '';
  public userResult = null;
  public addUserLoading = false;
  public loadingSearch = false;
  public isUserSearchError = false;
  @Input() contractId: string;
  @Output() afterAddUser: EventEmitter<void> = new EventEmitter<void>();

  constructor(
    private route: ActivatedRoute,
    private service: UserService,
    private modalService: NgbModal,
    public activeModal: NgbActiveModal,
    public translate: TranslateService
  ) {
  }

  ngOnInit() {}

  onInputChange(event) {
    this.isUserSearchError = false;
    const key = event.key;
    return (key >= 0 && key <= 9) || (key === 'Backspace' || key === 'Delete');
  }

  onAddUser(userIin) {
    this.addUserLoading = true;
    const data = {
      parties: [
        {
          iin: userIin
        }
      ]
    };
    this.service.addUser('/contracts', this.contractId, '/parties', data)
      .subscribe(res => {
        this.iinText = '';
        this.userResult = null;
        this.addUserLoading = false;
        this.activeModal.close();
        this.afterAddUser.emit();
      },
      error => {
        this.addUserLoading = false;
        this.userResult = null;
      }
    );

  }

  searchUser() {
    this.loadingSearch = true;
    this.isUserSearchError = false;
    this.service.searchUser('/dictionary/fullName', this.iinText)
      .subscribe(
        (res: any) => {
          this.userResult = {
            iin: res.elements[0].id,
            name: res.elements[0].name
          };
          this.loadingSearch = false;
        }, error => {
          this.isUserSearchError = true;
          this.loadingSearch = false;
        }
      );
  }
}
