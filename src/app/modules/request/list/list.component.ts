import {Component, OnInit} from '@angular/core';
import {IRequest, IRequestFilter, IRequestStatuses} from '../../../types/request.types';
import {ActivatedRoute} from '@angular/router';
import {RequestService} from '../request.service';
import {AuthService} from '../../../_services/auth.service';
import {NCAService} from '../../../_services/nca.service';
import {TranslateService} from '@ngx-translate/core';
import {ISignType} from '../../../types/common.types';
import {RequestTypes} from '../../../types/contract.types';
import {WithLocale} from '../../../types/request.types';
import {autoDownloadFile, PDF} from '../../share/utils/constants';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  public types = RequestTypes;
  public requestType = RequestTypes.outbox;
  public requestTypes = {
    outbox: {
      title: 'Outbox requests',
      desc: 'This section displays the status of applications for the collection and processing of personal data that you sent',
      actionBtns: null
    },
    inbox: {
      title: 'Inbox requests',
      desc: 'This section displays requests from third parties for the collection and processing of your data necessary for entering into a contractual relationship with you',
      actionBtns: [{title: 'Confirm'}, {title: 'Reject'}]
    }
  };
  public requests: IRequest[] = [];
  public filter: IRequestFilter = {};
  public loadingRequests = false;
  public linkRequest;
  public documentForSign;
  public certificateChosenSubs;
  public agreementText: WithLocale;
  public loadingDownload = {};

  constructor(
    private route: ActivatedRoute,
    private requestService: RequestService,
    private authService: AuthService,
    private ncaService: NCAService,
    private toastr: ToastrService,
    public translateService: TranslateService,
  ) {
    this.route.queryParams.subscribe(params => {
      this.requestType = params.requestType || RequestTypes.inbox;
      this.requests = [];
      this.linkRequest = undefined;
      this.getRequests(true);
    });
    this.certificateChosenSubs = this.ncaService.certificateChosen.subscribe(val => {
      if (val) {
        this.authService.updateUnreadAgreementCount(true);
        this.requests = [];
        this.linkRequest = undefined;
        this.getRequests(true);
      }
    });
  }

  ngOnInit() {
  }

  getRequests(update = false) {
    // this.filter.status = IRequestStatuses.CREATED;
    if (this.authService.isLoggedIn && !this.authService.user) {
      this.loadingRequests = true;
      this.authService.getProfile().subscribe(res => {
        this.getRequests(update);
      });
    }
    if (update) {
      this.requests = [];
    }
    if (this.requestType === RequestTypes.outbox) {
      this.filter.initiator =  this.authService.user.iin;
      delete this.filter.acceptor;
    } else {
      this.filter.acceptor = this.authService.user.iin;
      delete this.filter.initiator;
    }
    this.filter.limit = 10;
    this.filter.offset = 0;
    this.loadingRequests = true;
    (!this.linkRequest ? this.requestService.getRequestsByFilter(this.filter) : this.requestService.getRequestByLink(this.linkRequest))
      .subscribe((res: any) => {
        this.linkRequest = res.nextLink;
        this.requests = [...this.requests, ...res.requests];
        this.loadingRequests = false;
      },
      error => {
        this.loadingRequests = false;
      }
    );
  }

  onReject(requestId) {
    this.requestService.rejectRequest(requestId).subscribe(res => {
      this.toastr.info(this.translateService.instant('Statuses.REJECTED'));
      // TODO: Update contract list without getRequest
      this.requests = [];
      this.linkRequest = undefined;
      this.getRequests(true);
      this.authService.updateUnreadAgreementCount(true);
    });
  }

  onConfirm(event) {
    this.requestService.getXMLForSign(event.id)
      .subscribe((res: any) => {
        const {xml, agreement_text: agreementText} = res.result;
        this.ncaService.xmlToSign = xml;
        this.agreementText = agreementText;
        this.ncaService.selectSignType('SIGN', event.id, event.isInitiator, ISignType.Request);
      },
      error => {
        // this.toastr.error(this.translate.instant(error));
      });
  }

  onDownload(document) {
    const data = {
      agreementId: document.requestId
    };
    this.loadingDownload[document.requestId] = true;
    this.requestService.getDownloadLink(data)
      .subscribe((res: any) => {
        this.loadingDownload[document.requestId] = false;
        autoDownloadFile(res, PDF);
      },
      error => {
        // this.toastr.error(this.translate.instant(error));
      });
  }

  onScroll() {
    if (!this.loadingRequests && this.linkRequest) {
      this.getRequests();
    }
  }

  getActionBtns(request: IRequest) {
    if ((request.status === IRequestStatuses.REQUESTED || request.status === IRequestStatuses.CREATED) && !request.isInitiator) {
      return this.requestTypes[this.requestType].actionBtns;
    }
    return null;
  }

  getParticipantsText(request: IRequest) {
    if (request.isInitiator) {
      return `${this.translateService.instant('For')}: <span>${request.acceptorFullName[this.translateService.currentLang]}</span>`;
    }
    return `${this.translateService.instant('From')}: <span>${request.initiatorFullName[this.translateService.currentLang]}</span>`;
  }

}
