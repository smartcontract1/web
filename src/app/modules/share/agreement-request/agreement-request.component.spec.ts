import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgreementRequestComponent } from './agreement-request.component';

describe('AgreementRequestComponent', () => {
  let component: AgreementRequestComponent;
  let fixture: ComponentFixture<AgreementRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgreementRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgreementRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
