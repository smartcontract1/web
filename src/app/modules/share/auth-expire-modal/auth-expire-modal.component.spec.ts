import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthExpireModalComponent } from './auth-expire-modal.component';

describe('AuthExpireModalComponent', () => {
  let component: AuthExpireModalComponent;
  let fixture: ComponentFixture<AuthExpireModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthExpireModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthExpireModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
