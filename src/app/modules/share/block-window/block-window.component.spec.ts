import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlockWindowComponent } from './block-window.component';

describe('BlockWindowComponent', () => {
  let component: BlockWindowComponent;
  let fixture: ComponentFixture<BlockWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlockWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlockWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
