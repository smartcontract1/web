import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DateIntervalInputComponent } from './date-interval-input.component';

describe('DateIntervalInputComponent', () => {
  let component: DateIntervalInputComponent;
  let fixture: ComponentFixture<DateIntervalInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DateIntervalInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DateIntervalInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
