import { Component, Input, OnInit } from '@angular/core';

export enum DIAMETERS {
  extraSmall = '1.15rem',
  small = '1.5rem',
  medium = '3rem',
  large = '6rem',
}

export enum SIZES {
  extraSmall,
  small,
  medium,
  large
}

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent implements OnInit {
  @Input() size: SIZES = SIZES.medium;
  @Input() center = true;
  public diameter;

  constructor() {
    // TODO: Default diameter not works
    this.diameter = DIAMETERS[SIZES.medium];
  }

  ngOnInit() {
    this.diameter = DIAMETERS[this.size];
  }

}
