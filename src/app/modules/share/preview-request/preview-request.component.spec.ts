import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreviewRequestComponent } from './preview-request.component';

describe('PreviewRequestComponent', () => {
  let component: PreviewRequestComponent;
  let fixture: ComponentFixture<PreviewRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreviewRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviewRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
