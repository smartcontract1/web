import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {defaultDateTimeFormat} from '../utils/constants';
import {TranslateService} from '@ngx-translate/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-preview-request',
  templateUrl: './preview-request.component.html',
  styleUrls: ['./preview-request.component.css']
})
export class PreviewRequestComponent implements OnInit, OnChanges {
  public dateTimeFormat = defaultDateTimeFormat;
  @Input() id;
  @Input() link;
  @Input() title;
  @Input() state;
  @Input() companyName;
  @Input() commentary;
  @Input() createdDate;
  @Input() isInitiator: boolean;
  @Input() participants = [];
  @Input() actionBtns = null;
  @Input() active: boolean;
  @Input() showAgreementLink: boolean;
  @Input() loadingAgreementLink: boolean;
  @Input() stateLoading = false;
  @Output() confirm = new EventEmitter();
  @Output() reject = new EventEmitter();
  @Output() download = new EventEmitter();
  public loading = false;

  constructor(
    private translate: TranslateService,
    private modalService: NgbModal
  ) { }

  ngOnInit() {}
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.loading) {
      this.loading = changes.loading.currentValue;
    }
  }

  onConfirm() {
    this.loading = true;
    setTimeout(() => {
      this.loading = false;
    },3500);
    this.confirm.emit({id: this.id, isInitiator: this.isInitiator});
  }
  onOpenRejectModal(content) {
    this.modalService.open(content);
  }
  onCloseModal() {
    this.modalService.dismissAll();
  }
  onReject() {
    this.reject.emit(this.id);
    this.modalService.dismissAll();
  }
  onDownload() {
    this.download.emit(this.id);
  }
}
