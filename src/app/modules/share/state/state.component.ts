import {Component, Input, OnInit} from '@angular/core';
import {getStateColors} from '../utils/utils';

@Component({
  selector: 'app-state',
  templateUrl: './state.component.html',
  styleUrls: ['./state.component.css']
})
export class StateComponent implements OnInit {
  @Input() color: string;
  @Input() loading: boolean;

  constructor() { }

  ngOnInit() {
  }

  getStateColors() {
    return getStateColors(this.color);
  }
}
