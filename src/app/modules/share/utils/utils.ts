import {ContractStates, UserStatuses} from '../../../types/contract.types';
import {IRequestStatuses} from '../../../types/request.types';

export const getStateColors = (state) => {
  switch(state) {
    case ContractStates.DRAFT:
    case UserStatuses.NOT_SIGNED:
    case IRequestStatuses.CREATED:
      return '#949494';
    case ContractStates.IN_PROGRESS:
    case ContractStates.WAITING_FOR_APPROVAL:
    case ContractStates.WAITING_FOR_CONFIRMATION:
    case IRequestStatuses.REQUESTED:
      return '#F2C94C';
    case ContractStates.FAILED:
    case ContractStates.ARCHIVED:
    case ContractStates.CANCELED:
    case ContractStates.REJECTED:
    case IRequestStatuses.FINISHED:
    case IRequestStatuses.USED:
    case UserStatuses.REJECTED:
      return '#E54949';
    case ContractStates.SUCCESS:
    case ContractStates.DONE:
    case UserStatuses.SIGNED:
      return '#2DB366';
    default:
      return '#949494';
  }
};
