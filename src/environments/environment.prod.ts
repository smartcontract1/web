declare function require(moduleName: string): any;

const version = require('../../package.json').version;

export const environment = {
  production: true,
  authUrl: 'https://idp.egov.kz',
  api_url: 'https://api.sc.egov.kz/',
  old_api: 'https://api.sc.egov.kz/',
  api_version: 'v1',
  VERSION: version,
  domain: '.egov.kz',
  workflowUrl: 'https://api.sc.egov.kz',
  frontUrl: 'https://sc.egov.kz',
  egovUrl: 'https://my.egov.kz/',
  contractIDS: {
    PLEDGE_ID: 'd70497ff-2b8a-4434-afef-68651b97aea0',
    CESSION_ID: '9db0e7bf-490b-49c5-83af-7df0cd71bc9a',
    HOME_ID: 'c6515f1c-1690-4cf0-9b02-b028696ce7d1',
    AUTO_ID: '86671f52-c7f1-4334-b044-a645c2a84d40',
  },
  env: 'PROD'
};
