declare function require(moduleName: string): any;

const version = require('../../package.json').version;

export const environment = {
  production: false,
  authUrl: 'https://test.idp.egov.kz',
  api_url: 'http://api.test.sc.egov.kz/',
  old_api: 'http://api.test.sc.egov.kz/',
  api_version: 'v1',
  VERSION: version,
  domain: '.egov.kz',
  workflowUrl: 'http://api.test.sc.egov.kz',
  frontUrl: 'http://test.sc.egov.kz',
  egovUrl: 'http://test.my.egov.kz/',
  contractIDS: {
    PLEDGE_ID: '654e69a3-750f-438e-87f4-27edc3dc8785', // залог
    CESSION_ID: '0acb8a03-3eba-4122-a6b5-30ca9112e4d4',
    HOME_ID: 'dcb172ed-6d47-4ba0-b609-47c88d52efcf', // баспана
    AUTO_ID: 'a013436b-0d95-468f-b065-84f18e7b7c19',
  },
  env: 'TEST'
};
